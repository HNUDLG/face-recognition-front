'use strict'
import * as tf from '@tensorflow/tfjs-core'

export const disposeBox = (box) => {
  box.startEndTensor.dispose()
  box.startPoint.dispose()
  box.endPoint.dispose()
}

export const createBox = (startEndTensor) => ({
  startEndTensor,
  startPoint: tf.slice(startEndTensor, [0, 0], [-1, 2]),
  endPoint: tf.slice(startEndTensor, [0, 2], [-1, 2])
})

export const scaleBox = (box, factors) => {
  const starts = tf.mul(box.startPoint, factors)
  const ends = tf.mul(box.endPoint, factors)
  const newCoordinates = tf.concat2d([starts, ends], 1)
  return createBox(newCoordinates)
}
