import * as tfconv from '@tensorflow/tfjs-converter'
import * as face from './face.js'

const BLAZEFACE_MODEL_URL = process.env.VUE_APP_MODEL_URL

export async function load({ maxFaces = 10, inputWidth = 128, inputHeight = 128, iouThreshold = 0.3, scoreThreshold = 0.75 } = {}) {
  const blazeface = await tfconv.loadGraphModel(BLAZEFACE_MODEL_URL)
  return new face.BlazeFaceModel(blazeface, inputWidth, inputHeight, maxFaces, iouThreshold, scoreThreshold)
}
