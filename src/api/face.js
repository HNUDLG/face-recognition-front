import request from '@/utils/request'

export function faceRecognition(data) {
  return request({
    url: '/face/recognition',
    method: 'post',
    data: data,
    timeout: 30000
  })
}

export function fetchModels(taskId) {
  return request({
    url: 'face/models/' + taskId,
    method: 'get'
  })
}
