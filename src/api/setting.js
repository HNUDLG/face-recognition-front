import request from '@/utils/request'

export function fetchSettingValue(taskId, settingName) {
  return request({
    url: '/setting/value',
    method: 'get',
    params: { 'taskId': taskId, 'settingName': settingName }
  })
}
