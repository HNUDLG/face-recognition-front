import request from '@/utils/request'

export function addUser(data) {
  return request({
    url: '/user',
    method: 'post',
    data,
    timeout: 30000
  })
}

export function fetchUsers(data) {
  return request({
    url: '/users',
    method: 'get',
    params: data
  })
}

export function deleteUser(userId) {
  return request({
    url: '/user',
    method: 'delete',
    params: { 'id': userId }
  })
}
