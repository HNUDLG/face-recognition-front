import request from '@/utils/request'

export function fetchVideos(data) {
  return request({
    url: '/deepfake/videos',
    method: 'get',
    params: data
  })
}

export function deleteVideo(videoId) {
  return request({
    url: '/deepfake/video/' + videoId,
    method: 'delete'
  })
}

export function runDeepfakeDetection(videoId) {
  return request({
    url: '/deepfake/detection/' + videoId,
    method: 'get'
  })
}

export function detectionProgress(videoId) {
  return request({
    url: '/detection/progress/' + videoId,
    method: 'get'
  })
}
