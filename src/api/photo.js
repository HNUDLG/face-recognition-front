import request from '@/utils/request'

export function fetchPhotoNames(userId) {
  return request({
    url: '/photos/names',
    method: 'get',
    params: { 'id': userId }
  })
}
