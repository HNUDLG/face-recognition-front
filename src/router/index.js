import Vue from 'vue'
import Router from 'vue-router'
import { Layout } from '../layout'

Vue.use(Router)

export const constantRoutes = [
  {
    path: '*',
    redirect: '/404',
    hidden: true
  },
  {
    path: '/404',
    component: () => import('@/views/error-page/404'),
    hidden: true
  },
  {
    path: '/401',
    component: () => import('@/views/error-page/401'),
    hidden: true
  },
  {
    path: '',
    component: Layout,
    redirect: '/dashboard',
    children: [
      {
        path: 'dashboard',
        component: () => import('@/views/dashboard/index'),
        name: 'Dashboard',
        meta: { title: 'Dashboard', icon: 'dashboard' }
      }
    ]
  },
  {
    path: '/register',
    component: Layout,
    redirect: '/register/index',
    children: [
      {
        path: 'index',
        component: () => import('@/views/register/index'),
        name: 'Register',
        meta: { title: 'Register', icon: 'form' }
      }
    ]
  },
  {
    path: '/recognition',
    component: Layout,
    redirect: '/recognition/index',
    children: [
      {
        path: 'index',
        component: () => import('@/views/recognition/index'),
        name: 'Recognition',
        meta: { title: 'Recognition', icon: 'eye-open' }
      }
    ]
  },
  {
    path: '/detection',
    component: Layout,
    redirect: '/detection/component',
    meta: { title: 'Detection', icon: 'example' },
    children: [
      {
        path: 'component',
        component: () => import('@/views/detection/detection-component'),
        name: 'Component',
        meta: { title: 'Component' }
      },
      {
        path: 'blaze-face',
        component: () => import('@/views/detection/blaze-face'),
        name: 'BlazeFace',
        meta: { title: 'BlazeFace' }
      },
      {
        path: 'deepfake',
        component: () => import('@/views/detection/deepfake'),
        name: 'Deepfake',
        meta: { title: 'Deepfake' }
      }
    ]
  },
  {
    path: '/users',
    component: Layout,
    redirect: '/users/index',
    children: [
      {
        path: 'index',
        component: () => import('@/views/users/index'),
        name: 'Users',
        meta: { title: 'Users', icon: 'peoples' }
      }
    ]
  },
  {
    path: '/setting',
    component: Layout,
    redirect: '/setting/index',
    children: [
      {
        path: 'index',
        component: () => import('@/views/setting/index'),
        name: 'Setting',
        meta: { title: 'Setting', icon: 'list' }
      }
    ]
  }
]

const createRouter = () => new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router
