const state = {
  sidebar: {
    closed: false,
    withoutAnimation: false
  },
  size: 210
}

const getters = {
  isCollapse: state => state.sidebar.closed,
  menuWidth: state => state.size
}

const actions = {
  ToggleSideBar({ commit }) {
    commit('TOGGLE_SIDEBAR')
  }
}

const mutations = {
  TOGGLE_SIDEBAR: state => {
    state.sidebar.closed = !state.sidebar.closed
    state.sidebar.withoutAnimation = false
    if (state.sidebar.closed) {
      state.size = 64
    } else {
      state.size = 210
    }
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
