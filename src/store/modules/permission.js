import { constantRoutes } from '@/router'

const state = {
  routers: null,
  addRouters: [],
  topRouters: [],
  topTitle: '',
  menuIndex: 0
}

const getters = {
  permission_routers: state => state.routers
}

const actions = {
  GenerateRoutes({ commit }) {
    // console.log(constantRoutes)
    commit('SET_ROUTERS', constantRoutes)
  }
}

const mutations = {
  SET_ROUTERS: (state, routers) => {
    state.routers = routers
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
