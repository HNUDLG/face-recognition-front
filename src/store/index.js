import Vue from 'vue'
import Vuex from 'vuex'

import permission from './modules/permission'
import menu from './modules/menu'
import setting from './modules/setting'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    permission,
    menu,
    setting
  }
})
