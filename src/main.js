// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'

// a modern alternative to CSS resets
import 'normalize.css/normalize.css'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
// global css
import '@/styles/index.scss'

import App from './App'
import router from './router'
import store from './store/'

// icon
import './icons'

// global filters
import * as filters from './filters'

Vue.use(ElementUI, { size: 'medium' })
// Vue.component('icon-svg', IconSvg)

// register global utility filters
Object.keys(filters).forEach(key => {
  Vue.filter(key, filters[key])
})

Vue.config.productionTip = false

/* eslint-disable no-new */
const app = new Vue({
  router,
  store,
  // components: { App },
  render: h => h(App)
})
app.$mount('#app')
