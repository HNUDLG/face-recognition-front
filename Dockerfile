FROM node:18-alpine3.15 as builder

COPY . /var/www/src/face-recognition-front
WORKDIR /var/www/src/face-recognition-front

RUN npm install
RUN npm run build


FROM nginx:1.22 as production-build

RUN openssl req -x509 -nodes -days 365 -newkey rsa:2048  \
    -subj  "/C=CN/ST=SH/O=JoiZhang/CN=xinlin-04" \
    -keyout /etc/ssl/private/nginx-selfsigned.key  \
    -out /etc/ssl/certs/nginx-selfsigned.crt
COPY ./nginx/nginx.conf /etc/nginx/nginx.conf
RUN rm -rf /usr/share/nginx/html/*
COPY --from=builder /var/www/src/face-recognition-front/dist /usr/share/nginx/html

RUN mkdir /log

ENTRYPOINT ["nginx", "-g", "daemon off;"]